#include <cpp_redis/cpp_redis>
#ifdef _WIN32
#include <Winsock2.h>
#endif /* _WIN32 */
int main() {
#ifdef _WIN32
//! Windows netword DLL init
WORD version = MAKEWORD(2, 2);
WSADATA data;

if (WSAStartup(version, &data) != 0) {
    std::cerr << "WSAStartup() failure" << std::endl;
    return -1;
}
#endif /* _WIN32 */
    cpp_redis::active_logger = std::unique_ptr<cpp_redis::logger>(new cpp_redis::logger);
    cpp_redis::client client;
#ifdef _WIN32
	WSACleanup();
#endif /* _WIN32 */
    return 0;
}
