:: 基于 Windows Visual Studio 2015 的 cpp_redis 编译脚本
:: 可选参数:
::   /T              - 编译单元测试
::   /E              - 编译例子程序
::   /S              - 编译动态库,默认编译静态库
::   /MD             - 指定/MD编译选项,默认使用/MT

@ECHO OFF
SETLOCAL
ECHO make paho mqtt VS2015 project
IF NOT DEFINED VS140COMNTOOLS (
	ECHO vs2015 NOT FOUND.
	EXIT /B -1
)
ECHO vs2015 found.
WHERE cmake
IF ERRORLEVEL 1 (
	ECHO cmake NOT FOUND.
	EXIT /B -1
)
ECHO cmake found
SET sh_folder=%~dp0
:: 删除最后的 '\'
SET sh_folder=%sh_folder:~0,-1%

SET build_test=OFF
SET build_examples=OFF
SET build_shared=OFF
SET msvcrt_config=/MT
SET rel_suffix=_windows_vc_mt_x86-64
:: parse command arguments
:loop
IF x%1 == x GOTO :pare_end
IF /I "%1" == "/T"      SET build_test=ON
IF /I "%1" == "/E"      SET build_examples=ON
IF /I "%1" == "/S"      SET build_shared=ON
IF /I "%1" == "/MD"     SET msvcrt_config=/MD && SET rel_suffix=_windows_vc_md_x86-64

SHIFT
GOTO :loop
:pare_end

SET src_folder=cpp_redis
SET release_prefix=cpp_redis

SET project_folder=%sh_folder%\build\%release_prefix%.vs2015
IF "%OUTDIR%x" == "x" SET OUTDIR=%sh_folder%\release\%release_prefix%%rel_suffix%

ECHO project_folder=%project_folder%
ECHO OUTDIR=%OUTDIR%

:: 生成项目工程文件
IF EXIST %project_folder% RMDIR %project_folder% /s/q 
MKDIR %project_folder% 
PUSHD %project_folder% || EXIT /B

IF NOT DEFINED VisualStudioVersion (
	ECHO make MSVC environment ...
	CALL "%VS140COMNTOOLS%..\..\vc/vcvarsall" x86_amd64
)

ECHO creating x86_64 Project for Visual Studio 2015 ...
CMAKE %sh_folder%\%src_folder% -G "Visual Studio 14 2015 Win64" -DCMAKE_INSTALL_PREFIX=%OUTDIR% ^
	-DBUILD_SHARED_LIBS=%build_shared% ^
	-DBUILD_TESTS=%build_test% ^
	-DBUILD_EXAMPLES=%build_examples% ^
	-DMSVC_RUNTIME_LIBRARY_CONFIG=%msvcrt_config% || EXIT /B

cmake --build . --config Release --target install -- /maxcpucount || EXIT /B
cmake --build . --config Debug   --target install -- /maxcpucount || EXIT /B
POPD
ENDLOCAL