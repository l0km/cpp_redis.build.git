# cpp_redis.build

命令行编译 cpp_redis 

## Download

	git clone --recursive https://gitee.com/l0km/cpp_redis.build.git

## Build with MSVC

执行 `msvc_build.bat` 基于Visual Studio 2015 编译 cpp_redis

|可选参数|说明|
|:-|:-|
|/T|编译单元测试|
|/E|编译例子程序|
|/S|编译动态库,默认编译静态库|
|/MD|指定/MD编译选项,默认使用/MT|

## Build with with GCC/MinGW

执行 `gnu_build.sh` 基于GCC/MinGW 编译 cpp_redis


|可选参数|说明|
|:-|:-|
|ONLYBUILD|不执行 cmake 创建工程只编译版本|
|BUILD_TEST|编译单元测试|
|BUILD_EXAMPLES|编译例子程序|
|BUILD_SHARD|编译动态库,默认编译静态库|


可设置的的环境变量

|环境变量|说明|
|:-|:-|
|TOOLCHAIN_FILE|指定交叉编译的工具链文件|
|MACHINE|目标平台, 如 x86_64-linux-gnu,默认使用当前系统平台|
|PREFIX|安装路径|
|PROJECT_FOLDER|cmake 生成的工程文件(Makefile)文件夹|
|BUILD_TYPE|编译版本类型 DEBUG/RELEASE,默认 RELEASE|

## Conan Support

编译生成Conan包到本地仓库

```bash
$ conan create .
```

执行`conan upload`上传到私有制品库：

```bash
$ conan upload cpp_redis/master  -r ${repo} --all
# ${repo}为私有制品库的名字
```

