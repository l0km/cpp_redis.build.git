from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.scm import Git
from conan.tools.files import copy
from os.path import join

class CppRedisConan(ConanFile):
    name = "cpp_redis"
    ##########################################################
    # 这里根据需要可以填版本号,如4.3.1,也可以填git分支名     #
    # 填版本号,source()方法在克隆代码后会自动切换到指定的tag #
    # 填分支名,source()方法在克隆代码后会自动切换到指定的分支#
    ##########################################################
    version = "master"
    license = "MIT License"
    #url = "https://github.com/cpp-redis/cpp_redis"
    ##########################################################
    # 如果访问github较慢,可以使用我fork到到gitee.com的链接   #
    ##########################################################
    url = "https://gitee.com/l0km/cpp_redis"
    description = "C++11 Lightweight Redis client"
    topics = ("redis", "c++11","redis_client")
    package_type = "library"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "CMakeDeps","VirtualBuildEnv"
    ##########################################################
    # 克隆项目到本地并切换到指定的分支或tag(版本号)          #
    ##########################################################
    def source(self):
        git = Git(self)
        git.clone(self.url + ".git", args=['--branch', self.version,"--recursive"])

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["BUILD_TESTS"] = False
        tc.variables["BUILD_EXAMPLES"] = False
        if self.settings.compiler == "msvc" :
            ##########################################################
            # cpp_redis通过MSVC_RUNTIME_LIBRARY_CONFIG 变量控制代码  #
            # 的运行库(/MD或/MT),                                    #
            # 所以用conan定义的编译器属性compiler.runtime 来定义     #
            # MSVC_RUNTIME_LIBRARY_CONFIG                            #
            ##########################################################
            _runtime = str(self.settings.compiler.runtime)
            tc.variables["MSVC_RUNTIME_LIBRARY_CONFIG"] = "/" + _runtime[0:2]
        tc.generate()

    ##########################################################
    # 根据项目的实际情况定义cmake编译变量,编译项目           #
    ##########################################################
    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    ##########################################################
    # 项目打包,一旦编译成功执行,执行此方法完成项目打包       #
    # 生成项目的安装包                                       #
    ##########################################################
    def package(self):
        ##########################################################
        # cpp_redis 的CMakeLists.txt中的install命令生成的安装文件#
        # 目录结构不标准,没办法通过find_package查找,所以这里使用 #
        # copy命令生成标准的安装目录结构                         #
        # copy命令执行时src为基于build_folder文件夹的相对路径    #
        ##########################################################  	
        copy(self, "*",join(self.source_folder,"cpp_redis/includes"),join(self.package_folder,"include"))
        copy(self, "*", join(self.source_folder,"cpp_redis/tacopie/includes"), join(self.package_folder,"include"))
        copy(self, "*.lib", join(self.build_folder,"lib"),join(self.package_folder, "lib"),False)
        copy(self, "*.dylib", join(self.build_folder,"lib"),join(self.package_folder, "lib"),False)
        copy(self, "*.so", join(self.build_folder,"lib"),join(self.package_folder, "lib"),False)
        copy(self, "*.a", join(self.build_folder,"lib"),join(self.package_folder, "lib"),False)
        copy(self, "*.dll", join(self.build_folder,"lib"),join(self.package_folder, "bin"),False)

    ##########################################################
    # 根据项目的实际情况定义conan 安装包的构建信息           #
    ##########################################################	
    def package_info(self):
        self.cpp_info.libs = ["cpp_redis","tacopie"]
        if self.settings.os == "Windows" and self.settings.compiler == "gcc":
            # for MinGW
            self.cpp_info.system_libs = ["ws2_32"]
        elif self.settings.os in ["Linux", "FreeBSD"]:
            # for Unix
            self.cpp_info.system_libs = ["pthread"]